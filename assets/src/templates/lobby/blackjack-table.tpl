<div class="wrapper">
    <div class="link table-img">
        <div class="dealer">
            <h3 class="dealer-name"></h3>
            <div class="picture">
                <img src="/player/games/assets/lobby/dealers/nodealer.jpg" width="60" height="49" />
            </div>
        </div>
        <div class="image">
            <div class="counts"><span><%= this.getPlayersCount() %></span>/<span><%= attributes.maxPlayer %></span></div>
        </div>
        <div class="description"><%= attributes.title %> <span><%= this.getCurrencySymbol() %> <%- E.formatCurrency(attributes.tableMin) %> - <%- E.formatCurrency(attributes.tableMax) %></span></div>
    </div>
    <div class="header">
        <div class="col col1" data-i18n="lobby.table.information.mobile"></div>
    </div>
    <div class="contents">
        <div class="col col1 desc-dealer">
            <div class="description"><%- this.getDescription() %></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>