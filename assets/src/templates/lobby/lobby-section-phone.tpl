<section class="lobby <%= attributes.gameType %>">
    <h2 data-i18n="<%= attributes.gameType %>"><%= attributes.gameType %></h2>
    <div class="scrollable-lobby">
        <div class="wrap"></div>
    </div>
</section>