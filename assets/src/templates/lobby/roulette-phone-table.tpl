<div class="table lobby-table link">
    <div class="dealer-table-container">
        <div class="dealer">
            <div class="image"><img src="/player/games/assets/lobby/dealers/nodealer.jpg" width="94" height="78" /></div>
            <div class="name"></div>
        </div>
        <div class="table-image">
            <div class="image"></div>
        </div>
    </div>
    <div class="table-name"><%= attributes.title %></div>
    <div class="table-limits"><%= this.getCurrencySymbol() %> <%- E.formatCurrency(attributes.tableMin) %> - <%- E.formatCurrency(attributes.tableMax) %></div>
</div>