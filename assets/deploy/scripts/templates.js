define(function(){

this["templates"] = this["templates"] || {};

this["templates"]["templates/lobby/baccarat-table.tpl"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="wrapper">\n    <div class="link table-img">\n        <div class="dealer">\n            <h3 class="dealer-name"></h3>\n            <div class="picture">\n                <img src="/player/games/assets/lobby/dealers/nodealer.jpg" width="60" height="49" />\n            </div>\n        </div>\n        <div class="image"></div>\n        <div class="description">' +
((__t = ( attributes.title )) == null ? '' : __t) +
' <span>' +
((__t = ( this.getCurrencySymbol() )) == null ? '' : __t) +
' ' +
__e( E.formatCurrency(attributes.tableMin) ) +
' - ' +
__e( E.formatCurrency(attributes.tableMax) ) +
'</span>\n        </div>\n    </div>\n    <div class="header">\n        <div class="col col1" data-i18n="lobby.table.information.mobile"></div>\n        <div class="col col2" data-i18n="results"></div>\n        <div class="col col3"><span data-i18n="players"></span> (<span class="players-count">' +
((__t = ( this.getPlayersCount() )) == null ? '' : __t) +
'</span>)</div>\n    </div>\n    <div class="contents">\n        <div class="col col1 desc-dealer">\n            <div class="description">' +
__e( this.getDescription() ) +
'</div>\n        </div>\n        <div class="col col2 scorecard">\n            <div class="stats-list lobby">\n                <table>\n                    <tr class="scorecardTableLevel0 lobby ' +
((__t = ( attributes.tableId )) == null ? '' : __t) +
'">\n                    </tr>\n                    <tr class="scorecardTableLevel1 lobby ' +
((__t = ( attributes.tableId )) == null ? '' : __t) +
'">\n                    </tr>\n                    <tr class="scorecardTableLevel2 lobby ' +
((__t = ( attributes.tableId )) == null ? '' : __t) +
'">\n                    </tr>\n                    <tr class="scorecardTableLevel3 lobby ' +
((__t = ( attributes.tableId )) == null ? '' : __t) +
'">\n                    </tr>\n                    <tr class="scorecardTableLevel4 lobby ' +
((__t = ( attributes.tableId )) == null ? '' : __t) +
'">\n                    </tr>\n                    <tr class="scorecardTableLevel5 lobby ' +
((__t = ( attributes.tableId )) == null ? '' : __t) +
'">\n                    </tr>\n                </table>\n            </div>\n            <div class="loading hidden"></div>\n        </div>\n        <div class="col col3 players-container">\n            <ul class="players"></ul>\n            <div class="loading hidden"></div>\n        </div>\n        <div class="clear"></div>\n    </div>\n</div>\n<div class="clear"></div>';

}
return __p
};

this["templates"]["templates/lobby/blackjack-table.tpl"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="wrapper">\n    <div class="link table-img">\n        <div class="dealer">\n            <h3 class="dealer-name"></h3>\n            <div class="picture">\n                <img src="/player/games/assets/lobby/dealers/nodealer.jpg" width="60" height="49" />\n            </div>\n        </div>\n        <div class="image">\n            <div class="counts"><span>' +
((__t = ( this.getPlayersCount() )) == null ? '' : __t) +
'</span>/<span>' +
((__t = ( attributes.maxPlayer )) == null ? '' : __t) +
'</span></div>\n        </div>\n        <div class="description">' +
((__t = ( attributes.title )) == null ? '' : __t) +
' <span>' +
((__t = ( this.getCurrencySymbol() )) == null ? '' : __t) +
' ' +
__e( E.formatCurrency(attributes.tableMin) ) +
' - ' +
__e( E.formatCurrency(attributes.tableMax) ) +
'</span></div>\n    </div>\n    <div class="header">\n        <div class="col col1" data-i18n="lobby.table.information.mobile"></div>\n    </div>\n    <div class="contents">\n        <div class="col col1 desc-dealer">\n            <div class="description">' +
__e( this.getDescription() ) +
'</div>\n        </div>\n        <div class="clear"></div>\n    </div>\n</div>\n<div class="clear"></div>';

}
return __p
};

this["templates"]["templates/lobby/holdem-table.tpl"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="wrapper">\n    <div class="link table-img">\n        <div class="dealer">\n            <h3 class="dealer-name"></h3>\n            <div class="picture">\n                <img src="/player/games/assets/lobby/dealers/nodealer.jpg" width="60" height="49" />\n            </div>\n        </div>\n        <div class="image ' +
((__t = ( attributes.tableType )) == null ? '' : __t) +
'"></div>\n        <div class="description">' +
((__t = ( attributes.title )) == null ? '' : __t) +
' <span>' +
((__t = ( this.getCurrencySymbol() )) == null ? '' : __t) +
' ' +
__e( E.formatCurrency(attributes.tableMin) ) +
' - ' +
__e( E.formatCurrency(attributes.tableMax) ) +
'</span>\n        </div>\n    </div>\n    <div class="header">\n        <div class="col col1" data-i18n="lobby.table.information.mobile"></div>\n        <div class="col col2"><span data-i18n="players"></span> (<span class="players-count">' +
((__t = ( this.getPlayersCount() )) == null ? '' : __t) +
'</span>)</div>\n    </div>\n    <div class="contents">\n        <div class="col col1 desc-dealer">\n            <div class="description">' +
__e( this.getDescription() ) +
'</div>\n        </div>\n        <div class="col col2 players-container">\n            <ul class="players"></ul>\n            <div class="loading hidden"></div>\n        </div>\n        <div class="clear"></div>\n    </div>\n</div>\n<div class="clear"></div>';

}
return __p
};

this["templates"]["templates/lobby/lobby-section-phone.tpl"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<section class="lobby ' +
((__t = ( attributes.gameType )) == null ? '' : __t) +
'">\n    <h2 data-i18n="' +
((__t = ( attributes.gameType )) == null ? '' : __t) +
'">' +
((__t = ( attributes.gameType )) == null ? '' : __t) +
'</h2>\n    <div class="scrollable-lobby">\n        <div class="wrap"></div>\n    </div>\n</section>';

}
return __p
};

this["templates"]["templates/lobby/roulette-phone-table.tpl"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="table lobby-table link">\n    <div class="dealer-table-container">\n        <div class="dealer">\n            <div class="image"><img src="/player/games/assets/lobby/dealers/nodealer.jpg" width="94" height="78" /></div>\n            <div class="name"></div>\n        </div>\n        <div class="table-image">\n            <div class="image"></div>\n        </div>\n    </div>\n    <div class="table-name">' +
((__t = ( attributes.title )) == null ? '' : __t) +
'</div>\n    <div class="table-limits">' +
((__t = ( this.getCurrencySymbol() )) == null ? '' : __t) +
' ' +
__e( E.formatCurrency(attributes.tableMin) ) +
' - ' +
__e( E.formatCurrency(attributes.tableMax) ) +
'</div>\n</div>';

}
return __p
};

this["templates"]["templates/lobby/roulette-table.tpl"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="wrapper">\n    <div class="link table-img">\n        <div class="dealer">\n            <h3 class="dealer-name"></h3>\n            <div class="picture">\n                <img src="/player/games/assets/lobby/dealers/nodealer.jpg" width="60" height="49" />\n            </div>\n        </div>\n        <div class="image"></div>\n        <div class="description">' +
((__t = ( attributes.title )) == null ? '' : __t) +
' <span>' +
((__t = ( this.getCurrencySymbol() )) == null ? '' : __t) +
' ' +
__e( E.formatCurrency(attributes.tableMin) ) +
' - ' +
__e( E.formatCurrency(attributes.tableMax) ) +
'</span>\n        </div>\n    </div>\n    <div class="header">\n        <div class="col col1" data-i18n="lobby.table.information.mobile"></div>\n        <div class="col col2" data-i18n="results"></div>\n        <div class="col col3"><span data-i18n="players"></span> (<span class="players-count">' +
((__t = ( this.getPlayersCount() )) == null ? '' : __t) +
'</span>)</div>\n    </div>\n    <div class="contents">\n        <div class="col col1 desc-dealer">\n            <div class="description">' +
__e( this.getDescription() ) +
'</div>\n        </div>\n        <div class="col col2 stats">\n            <div class="stats-list"></div>\n            <div class="loading hidden"></div>\n        </div>\n        <div class="col col3 players-container">\n            <ul class="players"></ul>\n            <div class="loading hidden"></div>\n        </div>\n        <div class="clear"></div>\n    </div>\n</div>\n<div class="clear"></div>';

}
return __p
};

  return this["templates"];

});